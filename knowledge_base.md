# Knowledge Base
## Raspbian
To use docker and docker-compose with ansible on a raspbian, you must use python3 as ansible_python_interpreter and install
docker and docker-compose with pip3

```yml
---
- name: install python modules for docker and docker-compose
  pip:
    name: "{{ item.name }}"
    version: "{{ item.version }}"
    state: present
  with_items:
  - { name: docker, version: '>=1.8.0' }
  - { name: docker-compose, version: '>=1.7.0' }
```

```
raspbian ansible_python_interpreter=/usr/bin/python3 
```
----

To be able to get memory metrics on the pi you need to enable in `/boot/cmdline.txt` by including : `cgroup_memory=1 cgroup_enable=memory`.


## Docker Knowladge

Docker logs can be found with docker inspect -f {{.LogPath}} $HERE_IS_YOUR_CONTAINER
The defaut path with json-file log-driver looks like `/var/lib/docker/containers/<container-id>/<container-id>-json.log`

----

Example of docker with health check
 - i need to discover how to show it on grafana

```yml
- name: Start container with healthstatus
  docker_container:
    name: nginx-proxy
    image: nginx:1.13
    state: started
    healthcheck:
      # Check if nginx server is healthy by curl'ing the server.
      # If this fails or timeouts, the healthcheck fails.
      test: ["CMD", "curl", "--fail", "http://nginx.host.com"]
      interval: 1m30s
      timeout: 10s
      retries: 3
      start_period: 30s
```

docker-compose can describe dependence of containers:

```yml
web:
  image: example/my_web_app:latest
  depends_on:
    - db

db:
  image: postgres:latest
```

## Pihole

Pihole runs on docker, docker-compose used is similar to:
```yml
version: "3"

# More info at https://github.com/pi-hole/docker-pi-hole/ and https://docs.pi-hole.net/
services:
  pihole:
    container_name: pihole
    image: pihole/pihole:latest
    ports:
      - "53:53/tcp"
      - "53:53/udp"
      - "67:67/udp"
      - "80:80/tcp"
      - "443:443/tcp"
    environment:
      TZ: 'America/Sao_Paulo'
      # WEBPASSWORD: 'set a secure password here or it will be random' #TODO template to a vauted pass 
    # Volumes store your data between container upgrades
    volumes:
      - './etc-pihole/:/etc/pihole/'
      - './etc-dnsmasq.d/:/etc/dnsmasq.d/'
    dns:
      - 127.0.0.1
      - 1.1.1.1
    restart: unless-stopped
    # Recommended but not required (DHCP needs NET_ADMIN)
    #   https://github.com/pi-hole/docker-pi-hole#note-on-capabilities
    cap_add:
      - NET_ADMIN
```

# TODO

Configure source of all firewalls to the vpn range.
 
``` yml
- firewalld:
    source: 192.0.2.0/24
    zone: internal
    state: enabled
```


