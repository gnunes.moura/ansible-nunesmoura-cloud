# nunesmoura Cloud
Ansible Playbook to configure a private cloud. 
The main idea is to create a development lab with open-source application management.

The laboratory needs to offer remote secure access, with network visibility, log management, metrics and tracing capabilities.

## Usage
### Variables

| variable | default value | description |
| ---- | ---- | ---- |
| timezone |  America/Sao_Paulo | Time zone to be configured on services. |
| services_env_path |  /home/pi/services | Path to services env files on the target. |
| services_volume_path |  /home/pi/volumes | Path to services volume files on the target.  |
| main_domain |  nunes-moura.cloud | Base domain off all services |


### First Run

Tested running the Raspberry Pi OS (32-bit) with desktop and recommended software image from the official [download Raspberry Pi page][1]

**Prerequisites:**
- Using [raspi-config][2]:
    - Change password of the pi user;
    - Change Time Zone;
    - Change WLAN Country;
    - Expand `/` to use the full size of the micro-SD card.
- [Passwordless SSH access][3] to access the RPi;
- python3 interpreter exists on path `/usr/bin/python3`.


Using the [site-docker.yml][4] and a inventory like the example below, you will configure the Raspberry Pi to run Docker and docker-compose Ansible tasks:

```sh
raspbian ansible_host=192.168.0.2 ansible_user=pi ansible_python_interpreter=/usr/bin/python3
```

The `site-docker.yml` playbook must run without errors to ensure that all docker applications can be deployed.

```sh
ansible-playbook -i hosts site-docker.yml
```

Now the RPi is ready to run the services configured by the `site.yml` playbook.

```sh
ansible-playbook -i hosts site.yml
```

Finally SSH to the RPi and run `cd /home/pi/services && sudo docker-compose up -d`.


### DNS Configuration
1. Configure static IP for the RPi on the router, at this examples its 192.168.0.2
2. Configure the DHCP to assign the RPi as the DNS server
3. Add the list of addresses to the Pihole local DNS record
    - Domain = grafana.nunes-moura.cloud; IP Address = 192.168.0.2;
    - Domain = nodered.nunes-moura.cloud; IP Address = 192.168.0.2;
    - Domain = pihole.nunes-moura.cloud; IP Address = 192.168.0.2;
    - Domain = portainer.nunes-moura.cloud; IP Address = 192.168.0.2;

## Services
### Grafana 
[Grafana Official Site][6]


Grafana is open source visualization and analytics software. It allows you to query, visualize, alert on, and explore your metrics no matter where they are stored. In plain English, it provides you with tools to turn your time-series database (TSDB) data into beautiful graphs and visualizations.

Access on grafana.nunes-moura.cloud

Variables used by the service:
- timezone: Time zone to be configured.

Configured volumes:
- `{{ services_volume_path }}/grafana/data:/var/lib/grafana`
- `{{ services_volume_path }}/grafana/log:/var/log/grafana`

### NGINX 
[NGINX Official Site][7]

The open source web server that powers more than 400 million websites

Configure this list of addresses as sub domains of the `main_domain` on Pihole _Local DNS Records_ page pointing to the NGINX host:
- grafana.main_domain
- pihole.main_domain

Variables used by the service:
- main_domain: The main DNS to be used by NGINX.

Configured volumes:
- `{{ services_env_path }}/nginx/nginxconfig.io/:/etc/nginx/nginxconfig.io/`
- `{{ services_env_path }}/nginx/sites-available/:/etc/nginx/sites-available/`
- `{{ services_env_path }}/nginx/nginx.conf:/etc/nginx/nginx.conf`
- `{{ services_volume_path }}/nginx/log/:/var/log/nginx/`

### Node-RED 
[Node-RED Official Site][8]

Node-RED is a programming tool for wiring together hardware devices, APIs and online services in new and interesting ways.

It provides a browser-based editor that makes it easy to wire together flows using the wide range of nodes in the palette that can be deployed to its runtime in a single-click.

Access on http://nodered.nunes-moura.cloud

Variables used by the service:
- timezone: Time zone to be configured.

Configured volumes:
- `{{ services_volume_path }}/nodered/data:/data`


### PiHole 
[Node-RED Official Site][9]

PiHole is a network-wide DNS with Ad blocking.

Access on http://pihole.nunes-moura.cloud

Variables used by the service:
- timezone: Time zone to be configured.

Configured volumes:
- `{{ services_volume_path }}/pihole/etc-pihole/:/etc/pihole/`
- `{{ services_volume_path }}/pihole/etc-dnsmasq.d/:/etc/dnsmasq.d/`

### Portainer 
[Portainer Official Site][5]

Portainer Community Edition (Portainer CE) allows you to easily build, manage and maintain Docker environments (soon to include Kubernetes).

Access on http://portainer.nunes-moura.cloud

Configured volumes:
- `/var/run/docker.sock:/var/run/docker.sock`
- `{{ services_volume_path }}/portainer/data:/data`

### Prometheus
[Prometheus Official Site][10]

Prometheus is an open-source systems monitoring and alerting toolkit originally built at SoundCloud. Since its inception in 2012, many companies and organizations have adopted Prometheus, and the project has a very active developer and user community. It is now a standalone open source project and maintained independently of any company. To emphasize this, and to clarify the project's governance structure, Prometheus joined the Cloud Native Computing Foundation in 2016 as the second hosted project, after Kubernetes.

On this project we use the [CAdvisor][12] and [Node-Exporter][13] to expose metrics from the of the services.

Variables used by the service:
- main_domain
- rule_files
- scrape_configs

Configured volumes:
- `{{ services_env_path }}/prometheus/:/etc/prometheus/`
- `{{ services_volume_path }}/prometheus/:/prometheus/`

### Alertmanager 
[Alertmanager Official Site][11]
The Alertmanager handles alerts sent by client applications such as the Prometheus server. It takes care of deduplicating, grouping, and routing them to the correct receiver integration such as email, PagerDuty, or OpsGenie. It also takes care of silencing and inhibition of alerts.  

On this project we use the [prometheus-msteams][14] to integrate with the Microsoft Teams via the Incoming Webhook

Variables used by the service:
- teams_incoming_webhook_url

Configured volumes:
- '{{ services_env_path }}/alertmanager/:/etc/alertmanager/'

<!-- 
### Service Name http://link.algo
Description from site
Access on http://service.nunes-moura.cloud

Variables used by the service:
- foo
- baa

Configured volumes:
- asdf:/etc/service/
- asdf2:/etc/dnsmasq.d/ 
-->

[1]: https://www.raspberrypi.org/downloads/raspberry-pi-os/
[2]: https://www.raspberrypi.org/documentation/configuration/raspi-config.md
[3]: https://www.raspberrypi.org/documentation/remote-access/ssh/passwordless.md
[4]: https://gitlab.com/gnunes.moura/ansible-nunesmoura-cloud/-/blob/master/site-docker.yml
[5]: https://www.portainer.io/
[6]: https://grafana.com/
[7]: http://nginx.com
[8]: https://nodered.org/
[9]: https://pi-hole.net/
[10]: https://prometheus.io/
[11]: https://prometheus.io/docs/alerting/latest/alertmanager/
[12]: https://github.com/google/cadvisor
[13]: https://prometheus.io/docs/guides/node-exporter/
[14]: https://github.com/prometheus-msteams/prometheus-msteams
